var gulp = require('gulp');
var postcss = require('gulp-postcss');
var from = './src/main.css';
var to = './dist/css';

gulp.task('css', function () {
  var autoprefixer = require('autoprefixer');

  return gulp.src(from)
    .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
    .pipe(gulp.dest(to));
});


gulp.task('watch', function() {
  gulp.watch(from, ['css']);

});



gulp.task('default', ['css','watch']);
